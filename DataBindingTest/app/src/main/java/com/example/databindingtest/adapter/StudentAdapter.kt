package com.example.databindingtest.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.databindingtest.databinding.ItemBinding
import com.example.databindingtest.model.Student

class StudentAdapter(var list : List<Student>,
    var onClick :  (Int) -> Unit
) : RecyclerView.Adapter<StudentAdapter.StudentViewHolder>() {
    private var listStudent : List<Student> = list
    private var countViewHoler = 1


    inner class StudentViewHolder(binding : ItemBinding) : RecyclerView.ViewHolder(binding.root){
        var bd : ItemBinding
        init {
            bd = binding
            bd.tvAge.setOnClickListener{
                onClick(bd.tvAge.text.toString().toInt())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        var inflate = LayoutInflater.from(parent.context)
        var itemBinding = ItemBinding.inflate(inflate,parent,false)
        Log.e("onCreateViewHolder","onCreateViewHolder duoc tao " + countViewHoler.toString())
        countViewHoler++
        return StudentViewHolder(itemBinding)
    }

    override fun getItemViewType(position: Int): Int {
        Log.e("getItemViewType","getItemViewType duoc tao " + countViewHoler.toString())
        return super.getItemViewType(position)
    }




    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        var x = position + 1
        Log.e("onBindViewHolder","onBindViewHolder created = " + x.toString())
        var student = list.get(position)
        holder.bd.tvName.text = student.name
        holder.bd.tvAge.text = student.age

    }

    override fun getItemCount(): Int {
       return listStudent.size
    }




}