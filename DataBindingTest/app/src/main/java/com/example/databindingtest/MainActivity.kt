package com.example.databindingtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.LayoutManager
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.example.databindingtest.adapter.MultiTypeAdapter
import com.example.databindingtest.adapter.StudentAdapter

import com.example.databindingtest.databinding.ActivityMainBinding
import com.example.databindingtest.model.Animal
import com.example.databindingtest.model.Data
import com.example.databindingtest.model.Student

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var listStudent: List<Student>
    private lateinit var listAnimal: List<Animal>
    private lateinit var listData : List<Data>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
        multiRv()

    }
    private fun multiRv(){
        var adapter : MultiTypeAdapter = MultiTypeAdapter(listData)
        var layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = adapter
    }

    private fun setUpRecyclerView() {
        var listStudentAdapter = StudentAdapter(listStudent){
            Log.e("value",it.toString())
        }
        var layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = listStudentAdapter
    }

    private fun init() {
        listData = listOf(
            Student("A", "1"),
            Student("B", "2"),
            Student("C", "3"),
            Student("D", "4"),
            Student("E", "5"),
            Student("A", "6"),
            Animal("dog",4),
            Animal("Cat",4),
            Animal("boo",4),
            Student("B", "7"),
            Student("C", "8"),
            Student("D", "9"),
            Student("E", "10"),
            Student("A", "11"),
            Student("B", "12"),
            Animal("dog",4),
            Animal("Cat",4),
            Animal("boo",4),
            Student("A", "6"),
            Student("B", "7"),
            Student("C", "8"),
            Student("D", "9"),
            Student("E", "10"),
            Animal("people",2),
            Animal("duck",2),
            Animal("Elephant",4)
        )
        listStudent = listOf(
            Student("A", "1"),
            Student("B", "2"),
            Student("C", "3"),
            Student("D", "4"),
            Student("E", "5"),
            Student("A", "6"),
            Student("B", "7"),
            Student("C", "8"),
            Student("D", "9"),
            Student("E", "10"),
            Student("A", "11"),
            Student("B", "12"),
            Student("C", "13"),
            Student("D", "14"),
            Student("E", "15"),
            Student("A", "16"),
            Student("B", "17"),
            Student("C", "18"),
            Student("D", "19"),
            Student("E", "20"),
            Student("A", "21"),
            Student("B", "22"),
            Student("C", "23"),
            Student("D", "24"),
            Student("E", "25"),
            Student("B", "26"),
            Student("C", "27"),
            Student("D", "28"),
            Student("E", "29"),
        )

        listAnimal = listOf(
            Animal("dog",4),
            Animal("Cat",4),
            Animal("boo",4),
            Animal("people",2),
            Animal("duck",2),
            Animal("Elephent",4),
            Animal("Lion",4),
            Animal("fish",0),
            Animal("dog",4),
            Animal("Cat",4),
            Animal("boo",4),
            Animal("people",2),
            Animal("duck",2),
            Animal("Elephent",4),
            Animal("Lion",4),
            Animal("fish",0)
        )
    }
}