package com.example.databindingtest.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.databindingtest.databinding.ItemAnimalBinding
import com.example.databindingtest.databinding.ItemBinding
import com.example.databindingtest.model.Animal
import com.example.databindingtest.model.Data
import com.example.databindingtest.model.Student as Student

class MultiTypeAdapter (
    private val listData : List<Data>
        ) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class StudentViewHolder(itemBinding: ItemBinding) : RecyclerView.ViewHolder(itemBinding.root){
        val itemBinding : ItemBinding
        init {
            this.itemBinding = itemBinding
        }
    }

    inner class AnimalViewHolder(itemAnimalBinding: ItemAnimalBinding) : RecyclerView.ViewHolder(itemAnimalBinding.root){
        val animalBinding : ItemAnimalBinding
        init {
            this.animalBinding = itemAnimalBinding
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when(viewType){
            ViewType.TYPE_ONE.type ->{
                val itemBinding = ItemBinding.inflate(inflater,parent,false)
                return StudentViewHolder(itemBinding)
            }

            else -> {
                val animalBinding = ItemAnimalBinding.inflate(inflater,parent,false)
                return AnimalViewHolder(animalBinding)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        var data = listData[position]
        return when(data){
            is Student -> {
                Log.e("view1",ViewType.TYPE_ONE.type.toString())
                ViewType.TYPE_ONE.type
            }
            else -> {
                Log.e("view2",ViewType.TYPE_TWO.type.toString())
                ViewType.TYPE_TWO.type
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.apply {
            when(holder){
                is StudentViewHolder -> {
                    var data = listData[position]
                    when(data){
                        is Student -> {
                            holder.itemBinding.tvName.text = data.name
                            holder.itemBinding.tvAge.text = data.age
                        }
                    }

                }
                is AnimalViewHolder -> {
                    var animal = listData[position]
                    when(animal){
                        is Animal -> {
                            holder.animalBinding.tvTypeAnimal.text = animal.type
                            holder.animalBinding.tvFoots.text = animal.foots.toString()
                        }
                    }

                }
            }
        }

    }


    override fun getItemCount(): Int {
        return listData.size
    }

    enum class ViewType(val type : Int){
        TYPE_ONE(0),
        TYPE_TWO(1)
    }

}